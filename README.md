# Simple Django Shopping Cart using Django, PostgreSQL without Docker

## Installation on Windows OS
* PostgreSQL: https://www.enterprisedb.com/downloads/postgres-postgresql-downloads
* Git: https://git-scm.com/download/win
* Python: https://www.python.org/downloads/windows/
* Visual Studio Code: 

## Click PGAdmin4 Application 

> create "django_shopping_cart" with PGAdmin

## Open VSCode

## Check python and git installed

> git --version

> python --version

## Insatall Virtualenv of Python 

> python -m pip install --upgrade pip --user

> pip install virtualenv

## Dowload source code

> mkdir -p C:\django-workspace\

> cd C:\django-workspace\

> git clone https://gitlab.com/topic-tutorials/django-shopping-cart-with-postgres-without-docker.git

> cd django-shopping-cart-with-postgres-without-docker

## VSCode import "C:\django-workspace\django-shopping-cart-with-postgres-without-docker"

## Install dependent packages (Terminal of VSCode)
> python3 -m venv env

> env/Scripts/activate

> deactivate

> env/Scripts/activate

> python -m pip install --upgrade pip

> pip install -r requirements.txt

## Build Simple Django Shopping-Cart (Terminal of VSCode)

> cd C:\django-workspace\django-shopping-cart-with-postgres-without-docker\myshop

> python manage.py makemigrations

> python manage.py migrate

> python manage.py collectstatic

## Create Admin user of Simple Django Shopping-Cart (Terminal of VSCode)

> python manage.py createsuperuser

~~~
Username (leave blank to use 'root'): willis
Email address: misweyu2007@gmail.com
Password: 
Password (again): 
Superuser created successfully.
~~~

## Run Simple Django Shopping-Cart (Terminal of VSCode)

> python manage.py runserver 0.0.0.0:8001

## Surf Simple Django Shopping-Cart (Browser)

> http://localhost:8001

> http://localhost:8001/admin


## Use PGAdmin to show database (Browser)

> Click "PGAdmin4" Application > http://127.0.0.1:????